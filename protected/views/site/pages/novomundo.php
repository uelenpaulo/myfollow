<h2>Novo Mundo</h2>
<?php

$tvSamsung = new NovoMundo();
$tvSamsung->url = 'http://www.novomundo.com.br/loja/eletronicos/televisores?sL[marca]=samsung';
// $tvSamsung->description = 'Smart TV';

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $tvSamsung->dataProvider,
    'columns' => $tvSamsung->columns,
));

$tvLg = new NovoMundo();
$tvLg->url = 'http://www.novomundo.com.br/loja/eletronicos/televisores?sL[marca]=LG';

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $tvLg->dataProvider,
    'columns' => $tvLg->columns,
));

$refrigeradorBrastemp = new NovoMundo();
$refrigeradorBrastemp->url = 'http://www.novomundo.com.br/busca/refrigerador-brastemp-bre';
$refrigeradorBrastemp->arrInterests['product'] = array(
    'h2-product',
    'Inox',
    '220v',
);

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $refrigeradorBrastemp->dataProvider,
    'columns' => $refrigeradorBrastemp->columns,
));

$ar = new NovoMundo();
$ar->url = 'http://www.novomundo.com.br/busca/ar-condicionado-split/1/32/0/0/0';
$ar->arrInterests['product'] = array(
    'h2-product',
    'Ar',
);

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $ar->dataProvider,
    'columns' => $ar->columns,
));