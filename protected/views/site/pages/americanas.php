<h2>Americanas</h2>
<?php

$ps3 = new Americanas();
$ps3->url = 'http://www.americanas.com.br/linha/291067/games/console-playstation-3';

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $ps3->dataProvider,
    'columns' => $ps3->columns,
));

$gamesPs3 = new Americanas();
$gamesPs3->url = 'http://www.americanas.com.br/linha/291236/games/jogos-playstation-3?ofertas.limit=180';

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $gamesPs3->dataProvider,
    'columns' => $gamesPs3->columns,
));