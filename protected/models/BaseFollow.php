<?php
class BaseFollow extends CFormModel {

    public function getPageHtml($url = '') {

        if (empty($url)) { $url = $this->url; }

        $cURL = curl_init($url);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($cURL);
        curl_close($cURL);

        return $html;
    }

    public function strip_special_characters($str) {

        $out = "";
        for($i = 0; $i < strlen ( $str ); $i ++)
            if ((ord ( $str [$i] ) != 9) && (ord ( $str [$i] ) != 10) && (ord ( $str [$i] ) != 13))
                $out .= $str [$i];

        return $out;
    }

    public function utf8($str) {

        if (!mb_check_encoding($str, 'UTF-8')) {
            $str = utf8_encode($str);
        }

        return $str;
    }

    public function clearTag($html, $beginTag = '>', $endTag = '<') {

        $begin = strpos ( $html, $beginTag ) + strlen ( $beginTag );
        $end = strrpos ( $html, $endTag, - 1 ) - strlen ( $html );

        $html = substr ( $html, $begin, $end );

        return $html;
    }

    public function clear($arrProductDetails, $arrInterests = null) {

        if ($arrInterests == null) {

            $arrInterests = $this->arrInterests;
        }

        $clearedDetails = null;

        try {

            if (count($arrProductDetails) == 0) {

                return null;
            }

            foreach ( $arrProductDetails as $index => $product ) {

                $found = false;

                foreach ( $arrInterests as $key => $interest ) {

                    if (! is_array ( $interest )) {

                        $found = stripos ( $product, $interest );
                    } else {

                        foreach ( $interest as $interestInner ) {

                            $found = stripos ( $product, $interestInner );

                            if (! $found) {

                                break;
                            }
                        }
                    }

                    if ($found) {

                        break;
                    }
                }

                if (! $found) {

                    unset ( $arrProductDetails [$index] );
                } else {

                    $clearedDetails[$key] =$arrProductDetails [$index];
                }
            }
        } catch (Exception $e) {

            var_dump($arrProductDetails);
            exit;
        }

        return $clearedDetails;
    }
}