<?php
class NovoMundo extends BaseFollow {

    public $url = '';
    public $description = '';
    public $percDiscount = 0.10;

    public $arrInterests = array(
        'product' => array(
            'h2-product',
//             'Smart TV',
        ),
        'price' => array(
            'valor-por',
            'R$',
        ),
        'link' => array(
            'link-produto-titulo',
        ),
    );

    public $columns = array(
        'product',
        'price',
        'discount',
        array(
            'class' => 'CLinkColumn',
            'label' => 'link',
            'urlExpression' => '$data[\'link\']',
            'header' => 'link',
            'linkHtmlOptions' => array(
                'target' => 'blank',
            ),
        ),
    );

    public function getDataProvider() {

        $file = file_get_contents($this->url);

        $begin = strpos($file, 'id="produtos-departamento"') + 27;
        $end = strpos($file, 'class="clearfix resultados-paginacao paginator_nm_interface"') - strlen($file) - strlen('</div> </div> <hr class="linha-azul-1" /> <div ');

        $products = substr($file, $begin, $end);

        $arrProducts = split('<div class="item-produto span3">', $products);

        $gridProvider = array();

        foreach ($arrProducts as $index => $produto) {

            $produto = trim($produto);

            if (!empty($produto)) {

                $detalhes = split('><', self::strip_special_characters($produto));
                $detalhes = self::clear($detalhes);

                $arrProducts[$index] = $detalhes;

                if (count($detalhes) > 2) {

                    $gridProvider[$index]['product'] = self::clearTag($detalhes['product']);
                    $gridProvider[$index]['price'] = self::clearTag($detalhes['price'], 'strong>');

                    $price = self::clearTag($detalhes['price'], 'R$');
                    $price = preg_replace('/[^0-9,]/', '', $price);
                    $price = preg_replace('/[,]/', '.', $price);
                    $price = floatval($price);
                    $gridProvider[$index]['discount'] = round($price - $price * $this->percDiscount,2);

                    $gridProvider[$index]['link'] = self::clearTag($detalhes['link'], 'href="', '"');
                }
            }
        }

        return new CArrayDataProvider($gridProvider, array(
            'keyField' => 'product',
            'pagination' => array(
                'pageSize' => 1000,
            ),
            'sort' => array(
                'defaultOrder' => 'discount',
            )
        ));
    }
}