<?php
class Americanas extends BaseFollow {

    public $url = '';
    public $recordsPerPage = 180;
    public $description = '';
    public $generalDiscount = 0.05;

    public $arrInterests = array(
        'product' => array(
            '<a href="http://busca.americanas.com.br',
            'playstation 3',
        ),
        'price' => array(
            'loading-product-price',
        ),
    );

    public $columns = array(
        'product',
        'price',
        'discount',
        array(
            'class' => 'CLinkColumn',
            'label' => 'link',
            'urlExpression' => '$data[\'link\']',
            'header' => 'link',
            'linkHtmlOptions' => array(
                'target' => 'blank',
            ),
        ),
    );

    public function loadProductsPage($html) {

        $gridProvider = array();

        $dom = new DOMDocument();
        $dom->loadHTML($html);

        $xpath = new DOMXPath($dom);

        $items = $xpath->query('//div[@class="content"]/div[@class="paginado"]/section[@id="vitrine"]');

        if ($items->length > 0) {

            $products = array();

            $html = $dom->saveXML($items->item(0));

            $dom->loadHTML($html);
            $xpath = new DOMXPath($dom);
            $items = $xpath->query('//form');

            foreach ($items as $item) {

                $products[] = $dom->saveXML($item);
            }

            foreach ($products as $index => $product) {

                $gridProvider[$index]['product'] = '';
                $gridProvider[$index]['price'] = '';
                $gridProvider[$index]['discount'] = '';
                $gridProvider[$index]['link'] = '';

                $dom->loadHTML($product);
                $xpath = new DOMXPath($dom);

                $items = $xpath->query('//div[@class="productInfo"]/div[@class="top-area-product"]/a');

                if ($items->length > 0) {

                    $gridProvider[$index]['product'] = $this->utf8($items->item(0)->getAttribute('title'));
                    $gridProvider[$index]['link'] = $items->item(0)->getAttribute('href');
                }

                $items = $xpath->query('//div[@class="productInfo"]/div[@class="product-info"]/div[@class="price-area"]/span[@class="sale price"]/strong');

                if ($items->length > 0) {

                    $price = preg_replace('/[^0-9,]/', '', $items->item(0)->nodeValue);
                    $price = preg_replace('/[,]/', '.', $price);
                    $price = floatval($price);

                    $discount = $price - $price * $this->generalDiscount;

                    $gridProvider[$index]['price'] = $price;
                    $gridProvider[$index]['discount'] = round($discount,2);
                } else {

                    unset($gridProvider[$index]);
                }
            }
        }

        return $gridProvider;
    }

    public function getDataProvider() {

        $qtdeRegistros = 0;

        libxml_use_internal_errors(true);

        $cURL = curl_init($this->url);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($cURL);
        curl_close($cURL);

        $dom = new DOMDocument();

        $dom->loadHTML($html);

        $xpath = new DOMXPath($dom);
        $items = $xpath->query('//div[@class="content"]/div[@class="a-aux-box1"]');

        if ($items->length > 0) {

            $strCount = $items->item(0)->nodeValue;
            $qtdeRegistros = (int) substr($strCount, 0, strpos($strCount, ' '));
        }

        $gridProvider = self::loadProductsPage($html);

        $i = 0;
        while ($qtdeRegistros > $this->recordsPerPage) {

            $i++;
            $qtdeRegistros -= $this->recordsPerPage * $i;

            $html = $this->getPageHtml($this->url.'&ofertas.offset='.($this->recordsPerPage*$i));

            $gridProvider = array_merge($gridProvider, self::loadProductsPage($html));
        }

        libxml_use_internal_errors(false);

        return new CArrayDataProvider($gridProvider, array(
            'keyField' => 'product',
            'pagination' => array(
                'pageSize' => 1000,
            ),
            'sort' => array(
                'defaultOrder' => 'discount',
            )
        ));
    }
}